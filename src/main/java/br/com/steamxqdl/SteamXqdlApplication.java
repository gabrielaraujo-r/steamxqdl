package br.com.steamxqdl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SteamXqdlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SteamXqdlApplication.class, args);
	}

}