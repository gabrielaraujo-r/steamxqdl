package br.com.steamxqdl.service;

import java.util.List;

import br.com.steamxqdl.domain.Genero;

public interface GeneroService {

	void salvar(Genero genero);

	List<Genero> recuperar();

	Genero recuperarPorId(long id);

	void atualizar(Genero genero);

	void excluir(long id);

}