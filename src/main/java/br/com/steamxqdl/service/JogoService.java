package br.com.steamxqdl.service;

import java.util.List;

import br.com.steamxqdl.domain.Jogo;

public interface JogoService {

	void salvar(Jogo jogo, long generoId);

	List<Jogo> recuperarPorGenero(long generoId);

	Jogo recuperarPorGeneroIdEJogoId(long generoId, long jogoId);

	void atualizar(Jogo jogo, long generoId);

	void excluir(long generoId, long jogoId);

}