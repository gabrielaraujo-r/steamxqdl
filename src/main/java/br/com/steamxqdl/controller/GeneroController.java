package br.com.steamxqdl.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.steamxqdl.domain.Genero;
import br.com.steamxqdl.service.GeneroService;

@Controller
@RequestMapping("generos")
public class GeneroController {

	@Autowired
	private GeneroService generoService;

	@GetMapping("/listar")
	public ModelAndView listar(ModelMap model) {
		model.addAttribute("generos", generoService.recuperar());
		return new ModelAndView("/genero/list", model);
	}

	@GetMapping("/cadastro")
	public String preSalvar(@ModelAttribute("genero") Genero genero) {
		return "/genero/add";
	}

	@PostMapping("/salvar")
	public String salvar(@Valid @ModelAttribute("genero") Genero genero, BindingResult result,
			RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/genero/add";
		}

		generoService.salvar(genero);
		attr.addFlashAttribute("mensagem", "Gênero criado com sucesso.");
		return "redirect:/generos/listar";
	}

	@GetMapping("/{id}/atualizar")
	public ModelAndView preAtualizar(@PathVariable("id") long id, ModelMap model) {
		Genero genero = generoService.recuperarPorId(id);
		model.addAttribute("genero", genero);
		return new ModelAndView("/genero/add", model);
	}

	@PutMapping("/salvar")
	public String atualizar(@Valid @ModelAttribute("genero") Genero genero, BindingResult result,
			RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/genero/add";
		}

		generoService.atualizar(genero);
		attr.addFlashAttribute("mensagem", "Gênero atualizado com sucesso.");
		return "redirect:/generos/listar";
	}

	@GetMapping("/{id}/remover")
	public String remover(@PathVariable("id") long id, RedirectAttributes attr) {
		generoService.excluir(id);
		attr.addFlashAttribute("mensagem", "Gênero excluído com sucesso.");
		return "redirect:/generos/listar";
	}

}