package br.com.steamxqdl.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.steamxqdl.domain.Genero;

@Repository
public class GeneroDaoImpl implements GeneroDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public void salvar(Genero genero) {
		em.persist(genero);
	}

	@Override
	public List<Genero> recuperar() {
		return em.createQuery("select g from Genero g", Genero.class).getResultList();
	}

	@Override
	public Genero recuperarPorID(long id) {
		return em.find(Genero.class, id);
	}

	@Override
	public void atualizar(Genero genero) {
		em.merge(genero);
	}

	@Override
	public void excluir(long id) {
		em.remove(em.getReference(Genero.class, id));
	}

}